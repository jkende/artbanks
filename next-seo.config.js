const BASE_URL = 'https://artbanks.org';

const title = 'Artbanks';
const description = 'Decentralized market for art & rare goods';

const SEO = {
    title,
    description,
    canonical: `${BASE_URL}`,
    openGraph: {
        type: 'website',
        locale: 'en_US',
        url: `${BASE_URL}`,
        title,
        description,
        images: [
            {
                url: `${BASE_URL}/public/assets/og.png`,
                alt: title,
                width: 1280,
                height: 720
            }
        ]
    }
};

export default SEO;
