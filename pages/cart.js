import { useQuery } from '@apollo/client';
import Page from '../components/shop/page';
import EmptySection from '../components/shop/emptySection';
import Title from '../components/shop/title';
import FinishOrderCart from '../components/shop/finishOrderCart';
import ProductItem from '../components/shop/productItem';
import { CART, PRODUCTS_BY_IDS } from '../apollo/client/queries';
import ProductsGrid from '../components/shop/productsGrid';

export default function Profile() {
  const cart = useQuery(CART);

  const { data, loading, error } = useQuery(PRODUCTS_BY_IDS, {
    variables: {
      id: cart.data.cart.products,
    },
  });

  if (loading) return <></>;

  if (error)
    return (
      <Page>
        <Title title="Cart" />
        <EmptySection name="cart" />
      </Page>
    );

  return (
    <Page>
      <Title title="Cart" />
      <section className="cart">
        <aside>{data.productsById.length != 0 && <FinishOrderCart />}</aside>
        <div className="main">
          {!data?.productsById.length && <EmptySection name="cart" />}
          <ProductsGrid>
            {data?.productsById.map((product) => (
              <ProductItem
                key={product.id}
                id={product.id}
                name={product.name}
                rating={product.rating}
                img_url={product.img_url}
                price={product.price}
              />
            ))}
          </ProductsGrid>
        </div>
      </section>
    </Page>
  );
}
