import NextAuth from 'next-auth'
import Providers from 'next-auth/providers'
// import Adapters from 'next-auth/adapters'
import faunaAdapter from '../../../lib/fauna/fauna-adapter'

const options = {
    // Configure one or more authentication providers
    providers: [
        Providers.Email({
            server: process.env.EMAIL_SERVER,
            from: process.env.EMAIL_FROM,
        }),
        Providers.Google({
            clientId: process.env.GOOGLE_ID,
            clientSecret: process.env.GOOGLE_SECRET
        }),
        Providers.Twitter({
            clientId: process.env.TWITTER_ID,
            clientSecret: process.env.TWITTER_SECRET
        }),
        Providers.GitHub({
            clientId: process.env.GITHUB_ID,
            clientSecret: process.env.GITHUB_SECRET
        }),
    // ...add more providers here
    ],
    session: {
        jwt: true,
    },
    jwt: { secret: process.env.JWT_SECRET },
    debug: true,
    pages: {
        signIn: '/auth/signin',
        // error: '/auth/error',
        verifyRequest: '/auth/verify-request',
    },
    adapter: faunaAdapter.Adapter(null, {}),
}

export default (req, res) => NextAuth(req, res, options)