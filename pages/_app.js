import { Web3Provider } from "@ethersproject/providers"
import { Web3ReactProvider } from "@web3-react/core"
import Header from '../components/sections/header'
import Footer from '../components/sections/footer'
import Head from "next/head";
import { ChakraProvider, Box } from '@chakra-ui/core'
import theme from '@/design-system'
import { Provider } from 'next-auth/client'

function getLibrary(provider) {
  return new Web3Provider(provider);
}

export default function App({ Component, pageProps }) {
  return (
    <>
      <Head>
        <meta content="width=device-width, initial-scale=1" name="viewport" />
      </Head>
      <ChakraProvider resetCSS theme={theme} portalConfig={{ zIndex: 40 }}>
        <Web3ReactProvider getLibrary={getLibrary}>
          <Provider session={pageProps.session}>

            {/* <DefaultSeo {...SEO} /> */}
            <Box p={[0, 0, 10, 20]}>
              <Header />
              <Component {...pageProps} />
            </Box>
            <Footer />
          </Provider>
        </Web3ReactProvider>
      </ChakraProvider>
    </>
  );
}