import { verifyMessage } from "@ethersproject/wallet"
import { useWeb3React } from "@web3-react/core"
// import Head from "next/head"
// import Link from "next/link"
// import Account from "../components/Account"
// import ETHBalance from "../components/ETHBalance"
import useEagerConnect from "../hooks/useEagerConnect"
import usePersonalSign from "../hooks/usePersonalSign"

import {
    Box,
    Image,
    Flex,
    Text,
    SimpleGrid
} from '@chakra-ui/core'
// import { CgRepeat } from "react-icons/cg"

export default function Home() {
    const { account, library } = useWeb3React();

    // const triedToEagerConnect = useEagerConnect();

    const sign = usePersonalSign();

    // const handleSign = async () => {
    //     const msg = "Artbanks. A decentralized market.";
    //     const sig = await sign(msg);
    //     console.log("isValid", verifyMessage(msg, sig) === account);
    // };

    const isConnected = typeof account === "string" && !!library;

    return (
        <>
            <Flex
                display="flex"
                flexDirection="column"
                alignItems="center"
                justifyContent="center"
                textAlign="center"
                backgroundColor="black"
                // bgImage="url('https://images.unsplash.com/photo-1531913764164-f85c52e6e654?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2133&q=80')" 
                bgImage="url('/assets/meta/art/IMG_7189.JPG')"
                bgPos="center"
                bgRepeat="no-repeat"
                width="100%"
                height="3xl"
                mt={20}
            >
            </Flex>
            <Flex
                display="flex"
                flexDirection="column"
                alignItems="center"
                justifyContent="center"
                textAlign="center"
                mt={20}
                paddingY={320}
            >
                <Flex
                    display="flex"
                    flexDirection="row"
                    alignItems="flex-start"
                    justifyContent="flex-start"
                >
                    <Text fontSize={["xl", "2xl", "4xl", "6xl"]} fontWeight="bold">
                        NFT marketplace for art &amp; rare goods.
                    </Text>
                </Flex>
            </Flex>
            {/* <Grid p={10} gap={3} templateColumns="repeat(3, minmax(320px, 1fr))"> */}
            <SimpleGrid columns={[1, null, 3]} spacing="40px">
                <Box
                    display="flex"
                    flexDirection="column"
                    alignItems="center"
                    justifyContent="center"
                    p={10}
                >
                    <Box
                        bg="#ffffff"
                        width="sm"
                        minHeight="sm"
                        border="1px solid lightgrey"
                        overflow="hidden"
                        backgroundColor="red.400"
                    >
                        <Box>
                            <Image
                                size="100px"
                                // fallbackSrc="https://via.placeholder.com/320x520"                                
                                // src="/assets/meta/art/IMG_7186.JPG"
                                width="100%"
                                height="auto"
                                minHeight="560px"
                                maxHeight="560px"
                            />
                        </Box>
                    </Box>
                </Box>
                <Box
                    display="flex"
                    flexDirection="column"
                    alignItems="center"
                    justifyContent="center"
                    p={10}
                >
                    <Box
                        bg="#ffffff"
                        width="sm"
                        minHeight="sm"
                        border="1px solid lightgrey"
                        overflow="hidden"
                        backgroundColor="yellow.200"
                    >
                        <Box>
                            <Image
                                size="100px"
                                // fallbackSrc="https://via.placeholder.com/320x520"                                
                                src="/assets/meta/art/IMG_7188.JPG"
                                width="100%"
                                height="auto"
                                minHeight="560px"
                                maxHeight="560px"
                            />
                        </Box>
                    </Box>
                </Box>
                <Box
                    display="flex"
                    flexDirection="column"
                    alignItems="center"
                    justifyContent="center"
                    p={10}
                >
                    <Box
                        bg="#ffffff"
                        width="sm"
                        minHeight="sm"
                        border="1px solid lightgrey"
                        overflow="hidden"
                        backgroundColor="blue.200"
                    >
                        <Box>
                            <Image
                                size="100px"
                                // fallbackSrc="https://via.placeholder.com/320x520"                            
                                // src="/assets/meta/art/IMG_7137.jpg"
                                width="100%"
                                height="auto"
                                minHeight="560px"
                                maxHeight="560px"
                            />
                        </Box>
                    </Box>
                </Box>
                <Box
                    display="flex"
                    flexDirection="column"
                    alignItems="center"
                    justifyContent="center"
                    p={10}
                >
                    <Box
                        bg="#ffffff"
                        width="sm"
                        minHeight="sm"
                        border="1px solid lightgrey"
                        overflow="hidden"
                        backgroundColor="red.400"
                    >
                        <Box>
                            <Image
                                size="100px"
                                // fallbackSrc="https://via.placeholder.com/320x520"                            
                                src="/assets/meta/art/IMG_7186.JPG"
                                width="100%"
                                height="auto"
                                minHeight="560px"
                                maxHeight="560px"
                            />
                        </Box>
                    </Box>
                </Box>
                <Box
                    display="flex"
                    flexDirection="column"
                    alignItems="center"
                    justifyContent="center"
                    p={10}
                >
                    <Box
                        bg="#ffffff"
                        width="sm"
                        minHeight="sm"
                        border="1px solid lightgrey"
                        overflow="hidden"
                        backgroundColor="yellow.200"
                    >
                        <Box>
                            <Image
                                size="100px"
                                // fallbackSrc="https://via.placeholder.com/320x520"
                                // src="/assets/artbanks-logo-v1.gif"
                                width="100%"
                                height="auto"
                                minHeight="560px"
                                maxHeight="560px"
                            />
                        </Box>
                    </Box>
                </Box>
                <Box
                    display="flex"
                    flexDirection="column"
                    alignItems="center"
                    justifyContent="center"
                    p={10}
                >
                    <Box
                        bg="#ffffff"
                        width="sm"
                        minHeight="sm"
                        border="1px solid lightgrey"
                        overflow="hidden"
                        backgroundColor="blue.200"
                    >
                        <Box>
                            <Image
                                size="100px"
                                // fallbackSrc="https://via.placeholder.com/320x520"
                                // src="https://images.unsplash.com/photo-1469366379702-59bb9f45c11e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
                                width="100%"
                                height="auto"
                                minHeight="560px"
                                maxHeight="560px"
                            />
                        </Box>
                    </Box>
                </Box>
                <Box
                    display="flex"
                    flexDirection="column"
                    alignItems="center"
                    justifyContent="center"
                    p={10}
                >
                    <Box
                        bg="#ffffff"
                        width="sm"
                        minHeight="sm"
                        border="1px solid lightgrey"
                        overflow="hidden"
                        backgroundColor="red.400"
                    >
                        <Box>
                            <Image
                                size="100px"
                                // fallbackSrc="https://via.placeholder.com/320x520"
                                // src="https://images.unsplash.com/photo-1575995872537-3793d29d972c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
                                width="100%"
                                height="auto"
                                minHeight="560px"
                                maxHeight="560px"
                            />
                        </Box>
                    </Box>
                </Box>
                <Box
                    display="flex"
                    flexDirection="column"
                    alignItems="center"
                    justifyContent="center"
                    p={10}
                >
                    <Box
                        bg="#ffffff"
                        width="sm"
                        minHeight="sm"
                        border="1px solid lightgrey"
                        overflow="hidden"
                        backgroundColor="yellow.200"
                    >
                        <Box>
                            <Image
                                size="100px"
                                // fallbackSrc="https://via.placeholder.com/320x520"
                                // src="/assets/artbanks-logo-v1.gif"
                                width="100%"
                                height="auto"
                                minHeight="560px"
                                maxHeight="560px"
                            />
                        </Box>
                    </Box>
                </Box>
                <Box
                    display="flex"
                    flexDirection="column"
                    alignItems="center"
                    justifyContent="center"
                    p={10}
                >
                    <Box
                        bg="#ffffff"
                        width="sm"
                        minHeight="sm"
                        border="1px solid lightgrey"
                        overflow="hidden"
                        backgroundColor="blue.200"
                    >
                        <Box>
                            <Image
                                size="100px"
                                // fallbackSrc="https://via.placeholder.com/320x520"
                                // src="https://images.unsplash.com/photo-1469366379702-59bb9f45c11e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
                                src="/assets/meta/art/IMG_7137.jpg"
                                width="100%"
                                height="auto"
                                minHeight="560px"
                                maxHeight="560px"
                            />
                        </Box>
                    </Box>
                </Box>
            </SimpleGrid>
        </>
    );
}