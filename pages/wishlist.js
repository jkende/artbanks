import { useQuery } from '@apollo/client';
import Page from '../components/shop/page';
import EmptySection from '../components/shop/emptySection';
import Title from '../components/shop/title';
import AsideCategories from '../components/shop/asideCategories';
import { WISHLIST, PRODUCTS_BY_IDS } from '../apollo/client/queries';
import ProductsGrid from '../components/shop/productsGrid';
import ProductItem from '../components/shop/productItem';

export default function Wishlist() {
  const wishlist = useQuery(WISHLIST);

  const { data, loading, error } = useQuery(PRODUCTS_BY_IDS, {
    variables: {
      id: wishlist.data.wishlist.products,
    },
  });

  if (loading) return <></>;

  if (error || !data?.productsById.length)
    return (
      <Page>
        <Title title="Wishlist" />
        <EmptySection name="wishlist" />
      </Page>
    );

  return (
    <Page>
      <Title title="Wishlist" />
      <section className="wishlist">
        <aside>
          <AsideCategories />
        </aside>
        <div className="main">
          <ProductsGrid>
            {data?.productsById.map((product) => (
              <ProductItem
                key={product.id}
                id={product.id}
                name={product.name}
                rating={product.rating}
                img_url={product.img_url}
                price={product.price}
              />
            ))}
          </ProductsGrid>
        </div>
      </section>
    </Page>
  );
}
