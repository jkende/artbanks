import { useQuery } from '@apollo/client';
import { CATEGORIES } from '../../apollo/client/queries';
import {
  MdKeyboardArrowRight,
} from 'react-icons/md';
import Link from 'next/link';


const iconSlugs = {

};

export default function AsideCategories() {
  const { data, loading, error } = useQuery(CATEGORIES);

  if (loading) return <></>;

  if (error) return <></>;

  return (
    <ul className="categories">
      {data.categories.map((category) => {
        const Icon = iconSlugs[category.md_icon];
        return (
          <li key={category.id}>
            <Link href={`/category/${category.name}`}>
              <a>
                <div className="content">
                  <div className="icon">
                    <Icon color="#D8D8D8" size="22" />
                  </div>
                  <p>{category.label}</p>
                </div>
                <div className="arrow-button">
                  <MdKeyboardArrowRight color="#D8D8D8" size="26" />
                </div>
              </a>
            </Link>
          </li>
        );
      })}
      
    </ul>
  );
}
