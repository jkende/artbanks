import PageContainer from './page-container';
import Header from './../sections/header';
import Footer from './../sections/footer';

export default function Page({ title, description, children }) {
  return (
    <PageContainer title={title} description={description}>
      <Header />

      <div className="content">{children}</div>

      <Footer />
    </PageContainer>
  );
}
