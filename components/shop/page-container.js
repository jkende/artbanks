import Head from 'next/head';

export default function PageContainer({ title, description, children }) {
  return (
    <div className="container">
      <Head>
        <title>{title || 'Artbanks - Decentralized Commerce'}</title>
        {description !== false && (
          <meta
            name="description"
            content={
              description ||
              'Artbanks: Decentralized market for art & rare goods.'
            }
          />
        )}
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>{children}</main>
    </div>
  );
}
