import { Button } from "@chakra-ui/core"
import { useQuery } from '@apollo/client'
import { CART, PRODUCTS_BY_IDS_PRICE } from '../../apollo/client/queries'
import { useState } from 'react'

export default function FinishOrderCart() {
  const [finalPrice, setFinalPrice] = useState(0);
  const cart = useQuery(CART);

  const { data, loading, error } = useQuery(PRODUCTS_BY_IDS_PRICE, {
    variables: {
      id: cart.data.cart.products,
    },
  });

  if (loading) return <></>;

  if (error) return <></>;

  return (
    <div className="finishOrder">
      <div className="info">
        <p className="total">Total({cart?.data.cart.cartCount} Item):</p>
        <p className="price">$ {finalPrice}</p>
      </div>
      <Button>Finish Order</Button>
    </div>
  );
}
