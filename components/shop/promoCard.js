export default function PromoCard() {
  return (
    <div className="promo-card">
      <p className="title">Promo Title</p>
      <p className="description">Promo Description</p>
    </div>
  );
}
