import { sortProductSectionVar } from '../../apollo/client/cache';
import { useQuery } from '@apollo/client';
import { SORT_PRODUCT_SECTION } from '../../apollo/client/queries';

export default function HeaderBarProducts() {
  const { data } = useQuery(SORT_PRODUCT_SECTION);

  function handlePopularProductsClick() {
    sortProductSectionVar(['rating', 'DESC']);
  }
  function handleLowPriceProductsClick() {
    sortProductSectionVar(['price', 'ASC']);
  }
  function handleHighPriceProductsClick() {
    sortProductSectionVar(['price', 'DESC']);
  }

  return (
    <div className="header">
      <div className="sort-list">
        <a
          id="popular-products"
          className={
            data.sortProductSection[0] === 'rating' &&
            data.sortProductSection[1] === 'DESC'
              ? 'active'
              : ''
          }
          onClick={handlePopularProductsClick}
        >
          Popular products
        </a>
        <a
          id="low-price"
          className={
            data.sortProductSection[0] === 'price' &&
            data.sortProductSection[1] === 'ASC'
              ? 'active'
              : ''
          }
          onClick={handleLowPriceProductsClick}
        >
          Low price
        </a>
        <a
          id="high-price"
          className={
            data.sortProductSection[0] === 'price' &&
            data.sortProductSection[1] === 'DESC'
              ? 'active'
              : ''
          }
          onClick={handleHighPriceProductsClick}
        >
          High price
        </a>
      </div>
    </div>
  );
}
