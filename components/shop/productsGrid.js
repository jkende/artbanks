import { SimpleGrid } from "@chakra-ui/core";

export default function ProductsGrid({ children }) {
  return (
    <SimpleGrid>
      {children}
    </SimpleGrid>
  );
}
