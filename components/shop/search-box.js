import { FaSearch } from 'react-icons/fa';

export default function SearchBox() {
  return (
    <>
      <div className="search-box">
        <button className="search-button">
          <FaSearch color="#D8D8D8" size="15px" />
        </button>
        <input
          id="search"
          type="text"
          name="search"
          placeholder="Search goods"
        />
        <select id="categories-search" name="categories-search">
          <option value="" selected>
            Category
          </option>
          <option value="DB">DB</option>
        </select>
      </div>
    </>
  );
}
