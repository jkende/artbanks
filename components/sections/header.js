import { useQuery } from '@apollo/client';
import { signIn, signOut, useSession } from 'next-auth/client'
import Link from 'next/link'
import { Box, Flex, Text, Button, Stack, PseudoBox } from "@chakra-ui/core"
import { CgMenuGridO, CgCloseO } from 'react-icons/cg'
import {
  FaShoppingCart,
  FaRegHeart,
  FaUser,
  FaSignOutAlt,
  FaBars,
} from 'react-icons/fa';
import { CART_COUNT } from '../../apollo/client/queries';
// import SearchBox from '../search-box';
import SvgArtbanksBrandStreetTag from '../brand/ArtbanksBrandStreetTag'

const MenuItems = props => {
    const { children, isLast, to = "/", ...rest } = props
    return (
        <Text
            mb={{ base: isLast ? 0 : 8, sm: 0 }}
            mr={{ base: 0, sm: isLast ? 0 : 8 }}
            display="block"
            {...rest}
        >
            <Link href={to}>
                <a>{children}</a>
            </Link>
        </Text>
    )
}

const Header = props => {
    const [show, setShow] = React.useState(false)
    const toggleMenu = () => setShow(!show)
    const [ session, loading ] = useSession()

    return (
        <Flex
            as="nav"
            align="center"
            justify="space-between"
            wrap="wrap"
            w="100%"
            mb={8}
            paddingY={8}
            paddingX={[8, 8, 0, 0]}
            bg="transparent"
            color="primary.700"
            {...props}
        >
            <Flex align="center">
                <Box as={SvgArtbanksBrandStreetTag} />
            </Flex>

            <Box
                display={{ sm: show ? "block" : "none", md: "block" }}
                flexBasis={{ base: "100%", md: "auto" }}
            >
                <Flex
                    align={["center", "center", "center", "center"]}
                    justify={["center", "space-between", "flex-end", "flex-end"]}
                    direction={["column", "row", "row", "row"]}
                    pt={[4, 4, 0, 0]}
                >
                    {!session && <>
                        <MenuItems>
                            <Button onClick={signIn}>Sign in</Button>
                        </MenuItems>
                    </>}
                    {session && <>
                        <MenuItems isLast>
                            Signed in as {session.user.email}.<br />
                            <Button
                                onClick={signOut}
                                size="sm"
                                rounded="md"
                                color="white"
                                bg="black"
                                border="2px"
                                _hover={{
                                    bg: "white",
                                    border: "2px",
                                    color: "black",
                                }}
                            >
                                Sign out
                            </Button>
                        </MenuItems>
                    </>}
                </Flex>
            </Box>

            <Box display={{ sm: "block", md: "none" }} onClick={toggleMenu}>
                {show ? <CgCloseO /> : <CgMenuGridO />}
            </Box>
            
        </Flex>
    )
}

export default Header




// import Logo from '../logo';
// import SearchBox from '../search-box';

// export default function HeaderDesktop({ viewer }) {
//   const cart = useQuery(CART_COUNT);

//   return (
//     <>
//       <div className="header header-top">
//         <Logo />

//         <SearchBox />

//         <div className="nav-buttons">
//           <Link href="/cart">
//             <a className="nav-buttons-items">
//               <FaShoppingCart color="#808080" />
//               <p>
//                 <sup className="items-total">{cart.data.cart.cartCount}</sup>{' '}
//                 Items
//               </p>
//             </a>
//           </Link>
//           <Link href="/wishlist">
//             <a className="nav-buttons-wishlist">
//               <FaRegHeart color="#808080" />
//               <p>Wishlist</p>
//             </a>
//           </Link>
//           {!viewer && (
//             <Link href="/user/login">
//               <a className="nav-buttons-signin">
//                 <FaUser color="#808080" />
//                 <p>Sign In</p>
//               </a>
//             </Link>
//           )}
//           {viewer && (
//             <>
//               <Link href="/profile">
//                 <a className="nav-buttons-profile">
//                   <FaUser color="#808080" />
//                   <p>{viewer.name}</p>
//                 </a>
//               </Link>
//               <Link href="/user/signout">
//                 <a className="nav-buttons-signout">
//                   <FaSignOutAlt />
//                 </a>
//               </Link>
//             </>
//           )}
//         </div>
//       </div>
//       <div className="header header-bottom">
//         <div className="all-categories-box">
//           <FaBars color="#d8d8d8" />
//           <select name="categories" id="categories">
//             <option value="All Categories" selected>
//               All Categories
//             </option>
//             <option value="DB">DB</option>
//           </select>
//         </div>

//         <nav className="main-nav">
//           <Link href="#">
//             <a>Super Deals</a>
//           </Link>
//           <Link href="#">
//             <a>Featured Brands</a>
//           </Link>
//           <Link href="#">
//             <a>Collections</a>
//           </Link>
//           <Link href="#">
//             <a>Bestselling</a>
//           </Link>
//         </nav>

//         <div className="settings">
//           <div className="menu-dropdown">
//             <p>Help</p>
//           </div>
//           <div className="menu-dropdown">
//             <p>USD</p>
//           </div>
//           <div className="menu-dropdown">
//             <p>Language</p>
//           </div>
//         </div>
//       </div>

//     </>
//   );
// }
