import { Box } from "@chakra-ui/core"
import SvgArtbanksBrandStreetTag from '../brand/ArtbanksBrandStreetTag'

export default function Footer() {
    return (
        <>
            <Box
                p="0"
                alignItems="center"
                justifyContent="center"
                textAlign="center"
                backgroundColor="black"
                width="100%"
                height="xl"
                color="whitesmoke"
            >
                <SvgArtbanksBrandStreetTag />
            </Box>
        </>
    );
}
