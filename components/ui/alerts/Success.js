import {
    Alert,
    AlertIcon,
    AlertTitle,
    // AlertDescription,
} from "@chakra-ui/core";

export default function Alert({ message }) {
    return (
        <>
            <Alert status="error">
                <AlertIcon />
                <AlertTitle mr={2}>{message}</AlertTitle>
                {/* <AlertDescription>{messageDescription}</AlertDescription> */}
                <CloseButton position="absolute" right="8px" top="8px" />
            </Alert>
        </>
    );
}