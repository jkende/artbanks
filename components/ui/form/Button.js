import { Button } from "@chakra-ui/core"

export default function Button({ type, title }) {
    return (
      <>
        <Button type={type}>{title}</Button>
      </>
    );
}

// Check where and how this is used. Refactor to naming convention with greater clarity.
